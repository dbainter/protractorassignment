//--------------------------------------------------------------------------------------------------------
//-----------------------This Test Case was modified by Dakota w. Bainter on 05/31/2020-------------------
//-----------------------------This Test Case was created by someone at some point------------------------
//--------------------------------------------------------------------------------------------------------
describe('Protractor Demo App', function() {
  // = = = = = = = = = = = = = = = = = = = = = = = =
	//= = = = = = = = = testObjects = = = = = = = = =
  var firstNumber = element(by.model('first'));
  var secondNumber = element(by.model('second'));
  var operator = element(by.model('operator'));
  var goButton = element(by.id('gobutton'));
  var latestResult = element(by.binding('latest'));
  var history = element.all(by.repeater('result in memory'));
  var title = element.all(by.xpath("//h3[contains(text(), 'Super Calculator')]"));
	//= = = = = = = = = = = = = = = = = = = = = = = =
  // = = = = = = = = = = = = = = = = = = = = = = = =

  function solve(operatorValue,a, b) {
    operatorValue = operatorValue.toUpperCase() 
    var selectedOperator = element(by.xpath("//option[@value='"+operatorValue+"']"));
    firstNumber.sendKeys(a);
    secondNumber.sendKeys(b);
    operator.click()
    selectedOperator.click()
    goButton.click(); 
  }

  beforeEach(function() {
    browser.get('http://juliemr.github.io/protractor-demo/');
  });
  it('should have the title "Super Calculator"', function() {
    expect(title.isPresent())
  });

  it('should add one and two', function() { //add
    solve('addition',1, 2)
    expect(latestResult.getText()).toEqual('3');
  });

  it('should add four and six', function() { //add
    solve('addition', 4, 6)
    expect(latestResult.getText()).toEqual('10');
  });

  it('should divide six by three', function() { //divide
    solve('division', 6, 3)
    expect(latestResult.getText()).toEqual('2');
  })

  it('should read the value from an input', function() { //read from input
    firstNumber.sendKeys(1);
    expect(firstNumber.getAttribute('value')).toEqual('1');
  });

  it('should have a history', function() { //history
    solve('addition',1, 2);
    solve('addition',3, 4);

    expect(history.count()).toEqual(2);

    solve('addition', 5, 6);

    expect(history.count()).toEqual(3);
  });
});